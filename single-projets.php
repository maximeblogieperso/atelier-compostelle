<?php
get_header();

$tag = get_field('cat');
$d_cat = get_field('display-cat');
$intro = get_field('introduction');
$spec = get_field('specificite');
$pinterest = get_field('pinterest');

$quote = get_field('citation');
$contact_link = get_field('contact-link');
$contact_image = get_field('contact-image');

?>

	<section class="hero single-projet primary-color--bg">
		<div class="container">
			<div class="text-wrapper">
				<h1 class="title big-title">
					<?php the_title(); ?>
				</h1>
				<?php if( $d_cat == true ) : ?>
					<h2 class="sub-title medium-title"><?php echo $tag[0]; ?></h2>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="single-projet--main primary-color--bg">
		<div class="container">
			<div class="single-projet__desc row align-items-center flex-row-reverse">
				<!-- col -->
				<div class="col-md-12 col-lg-6 col-image">
					<div class="image-wrapper generic-vignette">
						<img src="<?php the_post_thumbnail_url() ?>" class="img-fluid" alt="<?php the_title(); ?> - <?php bloginfo( 'name' ); ?>">
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="text-wrapper custom-wysiwyg">
						<a href="<?php echo esc_url( get_page_link( 9 ) ); ?>" class="btn-link" title="Retour aux listing réalisations">
							Retour au listing
						</a>
						<div class="intro">
							<h3 class="medium-title title">A propos de la réalisation</h3>
							<?php if( $intro ) : ?>
								<p class="intro">
									<?php echo $intro; ?>
								</p>
							<?php endif; ?>
						</div>

						<ul class="list-item">
							<?php if ( $spec['client']) : ?>
								<li><strong>Client</strong>: <?php echo $spec['client']; ?></li>
							<?php endif;
							if ($spec['chief']) : ?>
								<li><strong>Chef de projet</strong>: <?php echo $spec['chief']; ?></li>
							<?php endif; 
							if ( $spec['year']) : ?>
								<li><strong>Année</strong>: <?php echo $spec['year']; ?> </li>
							<?php endif;

							if ( $spec['localisation']) : ?>
								<li><strong>Localisation</strong>: <?php echo $spec['localisation']; ?></li>
							<?php endif;

							if ( $spec['batiment']) : ?>
								<li><strong>Type de bâtiment</strong>: <?php echo $spec['batiment']; ?></li>
							<?php endif;

							if ( $spec['contexte']) : ?>
								<li><strong>Contexte</strong>: <?php echo $spec['contexte']; ?></li>
							<?php endif;

							if ( $spec['partenaire']) : ?>
								<li><strong>Partenaire</strong>: <?php echo $spec['batiment']; ?></li>
							<?php endif; 

							if ( $spec['copyright']) : ?>
								<li><strong>Photographie</strong>: <?php echo $spec['copyright']; ?></li>
							<?php endif; ?>
						</ul>
						<ul class="list-inline">
							<li>
								<a href="#galerie" class="btn btn-secondary--down" title="Découvrir la réalisation en images">
									Découvrir
								</a>
							</li>
							<li class="pinterest-btn">
								<a href="https://www.pinterest.com/pin/create/button/" rel="noopener" rel="nofollow" target="_blank" class="btn btn-icon" title="Enregistrer sur pinterest" data-pin-do="buttonBookmark" data-pin-custom="true">
									<i class="fa fa-pinterest"></i> Epingler
								</a>
							</li>
						</ul>
					</div>
				</div> <!-- end first col -->
			</div>
		</div>
	</section>

	<section id="galerie" class="single-projet--gallery">
		<div class="container">
			<?php if( have_rows( 'landscape' ) ) : ?>
				<div class="full-size row">
					<?php 
						while (have_rows( 'landscape' )) : the_row();
						$image = get_sub_field('image');
						$label = get_sub_field('label');
					?>
						<div class="item col-12">
							<div class="generic-vignette">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['url']; ?>"> 
								<?php if( $label != '' ) : ?>
									<span class="label">
										<?php echo $label; ?>
									</span>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div> <!-- end full size -->
			<?php endif; ?>

			<?php if( have_rows( 'portrait' ) ) : ?>
				<div class="half-size row">
					<?php 
						while (have_rows( 'portrait' )) : the_row();
						$image = get_sub_field('image');
						$label = get_sub_field('label');
					?>
						<div class="item col-md-6">
							<div class="generic-vignette">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['url']; ?>">
								<?php if( $label != '' ) : ?>
									<span class="label">
										<?php echo $label; ?>
									</span>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div> <!-- end half row -->
			<?php endif; ?>
		</div>
	</section>

	<section class="single-projet--cta primary-color--bg">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<?php if ( $contact_image ) : ?>
						<div class="image-wrapper">
							<img src="<?php echo $contact_image['url']; ?>" class="img-fluid" alt="<?php echo $contact_image['alt']; ?>">
						</div>
					<?php endif; ?>
				</div>

				<div class="col-md-6">
					<div class="text-wrapper">
						<?php if( $quote != '' ) : ?>
							<div class="element-quote white">
								<span class="title big-title">
									<?php echo $quote; ?>
								</span>
							</div>
						<?php endif; ?>

						<ul class="list-inline">
							<?php if ($contact_link) : ?>
							<li>
								<a href="<?php echo $contact_link['url']; ?>" class="btn btn-secondary--right" title="Contacter <?php bloginfo( 'name' ); ?>">
									Contacter l'atelier
								</a>
							</li>
							<?php endif; ?>
							<li>
								<a href="https://www.pinterest.com/pin/create/button/" rel="noopener" rel="nofollow" target="_blank" class="btn btn-icon" title="Enregistrer sur pinterest" data-pin-do="buttonBookmark" data-pin-custom="true">
									<i class="fa fa-pinterest"></i> Epingler
								</a>
							</li>
						</ul>
					</div>
				</div> <!-- end col -->
			</div>
		</div>
	</section>

<?php
	$args = array(  
        'post_type' => 'projets',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'orderby' => 'rand',
        'order' => 'ASC', 
    );
	$loop = new WP_Query( $args );
	?>

	<section class="single-projet--listing listing">
		<div class="primary-color--bg listing-intro">
			<div class="listing--container">
				<div class="row align-items-end">
					<div class="col-md-12 col-lg-6 col-xl-7">
						<div class="text-wrapper">
							<h3 class="big-title title">
								Découvrez plus de réalisations
							</h3>
						</div>
					</div>
					<div class="col-md-12 col-lg-6 col-xl-5">
						<div class="link-wrapper">
							<a href="<?php echo esc_url( get_page_link( 9 ) ); ?>" class="btn-link" title="Vers le listing projets <?php bloginfo( 'name' ); ?>">
								Toutes les réalisations
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="projets-list listing--container">
			<div class="row">
				<?php
					while ( $loop->have_posts() ) : $loop->the_post();
					$tag = get_field('cat');
					?>
					<article class="col-md-4 col-lg-4 listing--container__item">
						<a href="<?php the_permalink(); ?>" class="image-wrapper" title="Découvrir le projet <?php the_title(); ?>">
							<div class="generic-vignette">
								<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
								<div class="btn-arrow btn-arrow--right"></div>
							</div>
						</a>
						<div class="text-wrapper">
							<h4 class="title medium-title">
								<?php the_title(); ?>
							</h4>
							<?php if ( $tag ) : ?>
								<span class="desc">
									<?php echo $tag[0]; ?>
								</span>
							<?php endif; ?>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
		</div> <!-- end projects list -->
	</section>

<?php 
get_footer();
