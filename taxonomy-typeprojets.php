<?php 
	get_header(); 
	$pretitle_d = false; 

	$introduction  = get_field('introduction', 9);
	$main_title    = get_field('main-title', 9);
	$atelier       = get_field('atelier', 9);
	$atelier_image = get_field('atelier-image', 9);
	$my_id         = get_queried_object()->term_id;
	$slug          = get_queried_object()->slug;
?>

<?php include('components/hero.php'); ?>


<section id="projets" class="listing listing--projets archives">
	<div class="filters container">
		<div class="row align-items-center">
			<div class="filters__title col-xl-6 col-lg-12">
				<h2 class="primary-color big-title title max-width-lg">
					Transformations, rénovations et décoration d'intérieur
				</h2>
			</div>
			<div class="col-xl-6 col-lg-12">
				<?php
					$categories = get_categories( array(
						'taxonomy' => 'typeprojets',
	        			'post_status' => 'publish',
					    'orderby' => 'name',
					    'parent'  => 0
					) ); ?>
				<nav class="filters__nav">
					<ul>
						<li>
							<a id="all" href="<?php echo bloginfo('url') . '/realisations#projets'; ?>" class="btn-filters all" title="Tous les projets de <?php bloginfo( 'name' ); ?>">
								Tous les projets
							</a>
						</li>
						<?php foreach ( $categories as $category ) {
							$cat = $category->name;
							$cat_lower = strtolower($cat);
							$cat_link = $category->slug;
							if ( $category->term_id === $my_id ) {
								$my_class = 'active';
							} else {
								$my_class = '';
							}
						?>
							<li>
								<a id="<?php echo $cat_lower ; ?>"
								href="<?php echo bloginfo('url') . '/type-projet/' . $cat_link . '#projets'; ?>"
								class="btn-filters cat <?php echo $my_class; ?>">
									<?php echo $category->name; ?>
								</a>
							</li>
						<?php } ?>
						<?php wp_reset_postdata(); ?>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<div class="listing--container">
		<?php
			$args = array(  
	        'post_type' => 'projets',
	        'post_status' => 'publish',
	        'orderby' => 'menu_order', 
	        'order' => 'ASC', 
	        'posts_per_page' => 9,
	        'paged' => $paged,

	        'tax_query' => array(
		 		array(
		  			'taxonomy' => 'typeprojets', // le custom vocabulary des taxonomies
		 			'field'    => 'slug',                 
		 			'terms'    => $slug, // prend le premier slug
				),
			),
	    );

	    $loop = new WP_Query( $args ); 
		?>
		<div class="row">
			<?php
				while ( $loop->have_posts() ) : $loop->the_post();
				$cat_name = get_the_terms( $post->ID , 'typeprojets' );
				$first_cat = $cat_name[0]->name;
				$cat_name_lower = strtolower($first_cat);
				$sub_title      = get_field('cat');
			?>
	           <article class="<?php echo $cat_name_lower . '-type'; ?> all-type active listing--container__item col-md-6 col-lg-4">
					<a href="<?php the_permalink(); ?>" class="image-wrapper" title="Découvrir le projet <?php the_title(); ?>">
						<div class="generic-vignette">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<div class="btn-arrow btn-arrow--right"></div>
						</div>
					</a>
					<div class="text-wrapper">
						<h4 class="title medium-title">
							<?php the_title(); ?>
						</h4>
						<span class="desc">
							<?php echo $sub_title[0]; ?>
						</span>
					</div>
				</article>
		    <?php
			    endwhile;
			    wp_reset_postdata();
		    ?>
		</div>
		<?php
		    $total_pages = $loop->max_num_pages;
		    if ($total_pages > 1) :
		        $current_page = max(1, get_query_var('paged'));
		    ?>
		        <ul class="pagination-custom">
		        <?php echo paginate_links(array(
		            'base' => get_pagenum_link(1) . '%_%',
		            'format' => '/page/%#%',
		            'current' => $current_page,
		            'total' => $total_pages,
		            'prev_text'    => __('« précédente'),
		            'next_text'    => __('suivante »'),
		        ));
		        ?>
		        </ul>
		    <?php endif;  ?>
	</div>
</section>

<section class="listing--contact cta--banner generic-banner">
	<div class="container">
		<div class="cta--banner__wrapper row align-items-center flex-row-reverse primary-color--bg">
			<div class="col-md-12 col-lg-7 col-xl-6 offset-xl-1 text-wrapper">
				<?php
					$title      = $atelier['title'];
					$desc       = $atelier['desc'];
					$link_array = $atelier['link'];
					$link       = $link_array['url'];
					$label      = $link_array['title'];
					include('components/cta_text-block-simple.php');
				?>
			</div>
			<div class="col-md-12 col-lg-5">
				<div class="image-wrapper generic-vignette">
					<?php if( $atelier_image ) : ?>
						<a href="<?php $link; ?>" title="<?php echo $label; ?>">
							<img src="<?php echo $atelier_image['url']; ?>" class="img-fluid" alt="<?php echo $atelier_image['alt']; ?>">
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div> <!-- end container -->
	</div>
</section>

<?php get_footer(); ?>	