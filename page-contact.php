<?php 
/*
	Template Name: Page Contact
*/
get_header(); 
$pretitle_d = true; 

$insta     = get_field('link-instagram', 'option' );
$pinterest = get_field('link-pinterest', 'option' );

$introduction  = get_field('introduction');
$main_title    = get_field('main-title');
$contact_title = get_field('contact-title');
$contact_desc  = get_field('contact-intro');
$contact_form  = get_field('contact-form');
?>

<section class="hero hero--small-hero contact--intro primary-color--bg">
	<div class="container">
		<div class="text-wrapper">
			<h1 class="title big-title ">
				<?php echo $introduction['title']; ?>
			</h1>
			<?php if( $introduction['desc'] ) : ?>
				<p class="intro">
					<?php echo $introduction['desc']; ?>
				</p>
			<?php endif; ?>
			<ul class="btn-wrapper list-inline">
				<li class="list-inline-item">
					<a href="mailto:amandine.maroteaux@ateliercompostelle.com" class="btn-icon" target="_blank" title="Envoyer un mail à <?php bloginfo( 'name' ); ?>">
						<i class="fa fa-envelope-o"></i>
					</a>
				</li>
				
				<li class="list-inline-item">
					<a href="tel:+33 631 96 56 49" class="btn-icon" target="_blank" title="Appeler à <?php bloginfo( 'name' ); ?> Paris">
						<i class="fa fa-phone"></i>
					</a>
				</li>

				<li class="list-inline-item">
					<a href="tel:+352 691 272 796" class="btn-icon" target="_blank" title="Appeler à <?php bloginfo( 'name' ); ?> Luxembourg">
						<i class="fa fa-phone"></i>
					</a>
				</li>

				<li class="list-inline-item">
					<a href="https://www.google.com/maps/place/Boulevard+de+la+Foire,+1528+Luxembourg/data=!4m2!3m1!1s0x47954ed5ed706b49:0xd68856350e1bb882?sa=X&ved=2ahUKEwiDru_Gg-TtAhUxIcUKHR1iCB4Q8gEwAHoECAQQAQ" class="btn-icon" target="_blank" title="Accéder au plan <?php bloginfo( 'name' ); ?>">
						<i class="fa fa-map-o"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<section id="main" class="contact--main generic-banner white--bg">
	<div class="container">
		<div class="row flex-row-reverse">
			
			<div class="left-col col-md-12 col-lg-5">
				<div class="image-wrapper sticky-top">
					<div class="generic-vignette">
						<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt="<?php bloginfo( 'name' ); ?> Amandine Maroteaux">
					</div>
				</div>
			</div>

			<div class="left-col col-md-12 col-lg-7">
				<div class="cta cta--text-block custom-wysiwyg text-wrapper">
					<?php if( $contact_title != '' ) : ?>
						<h2 class="title big-title">
							<?php echo $contact_title; ?>
						</h2>
					<?php endif; ?>

					<ul class="btn-wrapper list-inline">
						<li class="list-inline-item">
							<a href="mailto:amandine.maroteaux@ateliercompostelle.com" class="btn-icon" target="_blank" title="Envoyer un mail à <?php bloginfo( 'name' ); ?>">
								<i class="fa fa-envelope-o"></i>
								<span>amandine.maroteaux@ateliercompostelle.com</span>
							</a>
						</li>

						<li class="list-inline-item">
							<a href="tel:+33 631 96 56 49" class="btn-icon" target="_blank" title="Appeler à <?php bloginfo( 'name' ); ?>">
								<i class="fa fa-phone"></i>
								<span>+33 631 96 56 49 (Paris)</span>
							</a>
						</li>

						<li class="list-inline-item">
							<a href="tel:+352 691 272 796" class="btn-icon" target="_blank" title="Appeler à <?php bloginfo( 'name' ); ?>">
								<i class="fa fa-phone"></i>
								<span>+352 691 272 796 (Luxembourg)</span>
							</a>
						</li>

						<li class="list-inline-item">
							<a href="https://www.google.com/maps/place/Boulevard+de+la+Foire,+1528+Luxembourg/data=!4m2!3m1!1s0x47954ed5ed706b49:0xd68856350e1bb882?sa=X&ved=2ahUKEwiDru_Gg-TtAhUxIcUKHR1iCB4Q8gEwAHoECAQQAQ" class="btn-icon" target="_blank" title="Accéder au plan <?php bloginfo( 'name' ); ?>">
								<i class="fa fa-map-o"></i>
								<span>Boulevard de la Foire 10, L.1528 Luxembourg</span>
							</a>
						</li>
						<li class="list-inline-item">
							<a href="https://www.google.com/maps/place/3+Rue+de+Montmorency,+75003+Paris,+France/@48.8624473,2.3548464,17z/data=!3m1!4b1!4m5!3m4!1s0x47e66e04c64c9507:0xf339b6ddb66809b4!8m2!3d48.8624473!4d2.3570351" class="btn-icon" target="_blank" title="Accéder au plan <?php bloginfo( 'name' ); ?>">
								<i class="fa fa-map-o"></i>
								<span>Rue de Montmorency 3, 75003 Paris</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="contact-form-wrapper">
					<?php echo do_shortcode($contact_form); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- post insta -->
<?php include_once('components/slider-insta.php'); ?>

<?php get_footer(); ?>	