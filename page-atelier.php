<?php 
/*
	Template Name: Page Atelier
*/
get_header(); 
$pretitle_d = true; 

$introduction     = get_field('introduction');
$main_title       = get_field('main-title');
$conception       = get_field('conception');
$conception_image = get_field('conception-image');
$citation         = get_field('citation');
$citation_image   = get_field('citation-image');
$realisations     = get_field('realisations');
$citation_red     = get_field('citation-red');
$citation_desc    = get_field('citation-desc');
$team_title       = get_field('team-title');
$bureau           = get_field('bureau');
?>

<?php include('components/hero.php'); ?>

<section id="main" class="atelier--main first generic-banner white--bg">
	<div class="container">
		<?php if( $main_title != '' ) : ?>
			<div class="max-width-lg">
				<h2 class="title big-title">
					<?php echo $main_title; ?>
				</h2>
			</div>
		<?php endif; ?>
		<div class="atelier__intro align-items-center row flex-row-reverse">
			<div class="text-wrapper col-md-12 col-lg-6 offset-lg-1 col-xl-5">
				<?php
					$pretitle_d = true; 
					$pre_title  = $conception['pretitle'];
					$title      = $conception['title'];
					$desc       = $conception['desc'];
					$link       = '#philosophie';
					$label      = $conception['label'];
					include('components/cta_text-block.php');
				?>
			</div>
			<div class="col-md-12 col-lg-5 col-xl-6">
				<?php if ( $conception_image ) : ?>
					<div class="image-wrapper generic-vignette">
						<img src="<?php echo $conception_image['url']; ?>" class="img-fluid" alt="<?php echo $conception_image['alt']; ?>">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php if ( $citation_red || $citation_desc ) : ?>
<section class="atelier--quote generic-banner">
	<div class="container">
		<div class="atelier__philosophie row align-items-center">
			<div class="col-md-12 col-lg-6">
				<?php if( $citation_red ) : ?>
					<div class="text-wrapper quote-wrapper">
						<div class="element-quote">
							<blockquote class="medium-title title secondary-color">
								<?php echo $citation_red; ?>
							</blockquote>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-12 col-lg-5 offset-xl-1">
				<?php if( $citation_desc ) : ?>
					<div class="text-wrapper custom-wysiwyg">
						<p>
							<?php echo $citation_desc; ?>
						</p>
					</div>
				<?php endif ;?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if (have_rows('team') ) : ?>
	<section class="atelier--team white--bg">
		<div class="container-fluid">
			<div id="team" class="atelier__team row">
				<?php if( $team_title) : ?>
					<div class="col-md-12">
						<div class="container">
							<div class="row">
								<div class="col-12">
									<h3 class="title medium-title">
										<?php echo $team_title; ?>
									</h3>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="horizontal-slider-wrapper image-wrapper col-12">
					<span id="team-prev" class="team-prev-btn slider-next-btn" title="Scroller vers la gauche"></span>
					<span id="team-next" class="team-next-btn slider-prev-btn" title="Scroller vers la droite"></span>
					<div id="team-slider" class="horizontal-slider team-slider disable-scrollbars">
					<?php while(have_rows('team') ) : the_row();
						$image    = get_sub_field('photo'); 
						$name     = get_sub_field('name');
						$fonction = get_sub_field('function');
						$locate   = get_sub_field('localisation');
						$mail     = get_sub_field('mail');
						?>
						<article class="slider-item team-slider--item">
							<div class="image-wrapper generic-vignette">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid">
							</div>
							<div class="text-wrapper">
								<span class="title small-title">
									<?php echo $name; ?>
								</span>
								<?php if ( $fonction ) : ?>
									<span class="desc">
										<?php echo $fonction; ?>
									</span>
								<?php endif;
								if ( $locate || $mail ) : ?>
									<span class="locate">
										<?php echo $locate; ?><br/>
										<?php echo $mail; ?>
									</span>
								<?php endif; ?>
							</div>
						</article>
					<?php endwhile; ?>
					</div>
				</div> <!-- end horizontal scroll -->
			</div>
		</div>
	</section>
<?php endif; ?>

<section id="bureau" class="atelier--bureau primary-color--bg generic-banner">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="text-wrapper col-lg-6">
				<?php
					$pretitle_d = false; 
					$pre_title  = '';
					$title      = $bureau['title'];
					$desc       = $bureau['desc'];
					$link       = $bureau['link']['url'];
					$label      = $bureau['link']['title'];
					include('components/cta_text-block.php');
				?>
			</div>
			<?php if ( have_rows('bureau-image') ) : ?>
				<div class="horizontal-slider-wrapper image-wrapper col-lg-6 pr-0">
					<div id="projet-slider" class="horizontal-slider disable-scrollbars">
						<?php
							while( have_rows( 'bureau-image' ) ) : the_row();
							$image = get_sub_field('image');
						?>
					           <article class="slider-item projet-slider-item vignette-projet">
									<a href="<?php echo $bureau['link']['url']; ?>" title="Découvrir les bureaux de l'atelier">
										<img src="<?php echo $image['url']; ?>" class="img-logo" alt="<?php echo $image['alt']; ?>">
									</a>
								</article>
					    <?php
						    endwhile;
					    ?>
					</div> <!-- end horizontal scroll -->

					<span id="projet-prev" class="slider-prev-btn" title="Scroller vers la gauche"></span>
					<span id="projet-next" class="slider-next-btn" title="Scroller vers la droite"></span>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>


<?php
	$args = array(  
        'post_type' => 'projets',
        'post_status' => 'publish',
        'posts_per_page' => 1,
        /*'orderby' => 'menu_order',  */
        'orderby' => 'date_post',
        'order' => 'ASC', 
    );
	$loop = new WP_Query( $args );
?>

<section class="atelier--contact cta--banner primary-color--bg">
	<div class="container">
		<div class="cta--banner__wrapper row flex-row-reverse align-items-center">
			<div class="col-md-12 col-lg-6 col-xl-6 offset-xl-1 text-wrapper">
				<?php
					$title      = $realisations['title'];
					$desc       = $realisations['desc'];
					$link_array = $realisations['link'];
					$link       = $link_array['url'];
					$label      = $link_array['title'];
					include('components/cta_text-block-simple.php'); ?>
			</div>
			<div class="col-left col-md-12 col-lg-6 col-xl-5">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<article class="image-wrapper generic-vignette vignette-projet">
						<a href="<?php the_permalink(); ?>" title="Découvrir le projet <?php the_title(); ?>">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<span class="label btn-white--right">
								<?php the_title(); ?>
							</span>
						</a>
					</article>
				<?php endwhile; ?>
			</div>
		</div> <!-- end container -->
	</div>
</section>

<section id="philosophie" class="atelier--main white--bg generic-banner">
	<div class="container">
		<div class="atelier__philosophie row align-items-center">
			<div class="col-left col-md-12 col-lg-7">
				<div class="text-wrapper element-quote">
					<?php if( $citation ) : ?>
						<blockquote class="medium-title title">
							<?php echo $citation; ?>
						</blockquote>
					<?php endif; ?>
					<span class="secondary-color signature">
						Amandine Maroteaux, directrice de création
					</span>
				</div>
			</div>
			<div class="col-right col-md-12 col-lg-5 col-xl-4">
				<div class="image-wrapper ">
					<?php if( $citation_image ) : ?>
						<img src="<?php echo $citation_image['url']; ?>" class="img-fluid" alt="<?php echo $citation_image['alt']; ?>">
					<?php endif ;?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- post insta -->
<?php include_once('components/slider-insta.php'); ?>

<?php get_footer(); ?>	