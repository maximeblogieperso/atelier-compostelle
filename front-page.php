<?php
get_header();

$pretitle_d = true;

$realisations   = get_field('realisations');
$atelier        = get_field('atelier');
$atelier_image  = get_field('atelier-image');
$citation       = get_field('citation');
$citation_image = get_field('citation-image');
$contact        = get_field('contact');
$contact_image  = get_field('contact-image');
?>

	<section class="hero hero--home generic-banner">
		<div class="container">
			<div class="row">
				<div class="col-left col-lg-6 col-xl-5">
					<div class="text-wrapper">
						<img src="<?php echo esc_url(home_url()); ?>/images/logo-white-alt.svg" class="logo" alt="<?php bloginfo( 'name' ); ?> logo"> 
						<h2 class="small-title max-width-sm centered">
							Interior Design<br/>
							Paris - Luxembourg
						</h2>
						<p>
							<a href="#projets" class="btn-secondary--down" title="Découvrir les projets de L'atelier">
								Découvrir
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="image-wrapper">
			<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php bloginfo( 'name' ); ?> Bureau architecture"> 
		</div>
	</section>

	<?php include ('components/slider-projet.php'); ?>

	<!-- Atelier banner -->
	<section class="home--atelier two-column-banner generic-banner white--bg">
		<div class="container">
			<div class="row flex-row-reverse align-items-start">
				<?php if( $atelier_image ) : ?>
					<div class="right-col col-12 col-lg-6">
						<div class="image-wrapper generic-vignette">
							<a href="<?php echo $link; ?>" title="<?php echo $label; ?>">
								<img src="<?php echo $atelier_image['url']; ?>" class="img-fluid" title="<?php echo $atelier_image['alt']; ?>">
							</a>
						</div>
					</div>
				<?php endif; ?>
				<div class="text-wrapper left-col col-12 col-lg-6">
					<?php
						$pre_title  = $atelier['pretitle'];
						$title      = $atelier['title'];
						$desc       = $atelier['desc'];
						$link_array = $atelier['link'];
						$link       = $link_array['url'];
						$label      = $link_array['title'];
						include('components/cta_text-block.php');
					?>
				</div>
			</div>
		</div>	
	</section>

	<!--
	<section class="home--quote quote-banner" style="display: none;">
		<div class="container">
			<div class="row align-items-lg-center align-items-xl-end">
				<div class="left-col col-md-5 col-lg-5 col-xl-4">
					<?php // if($citation_image) : ?>
						<div class="image-wrapper">
							<img src="<?php // echo $citation_image['url']; ?>" class="img-fluid" alt="<?php // echo $citation_image['alt']; ?>">
						</div>
					<?php // endif; ?>
				</div>
				<div class="text-wrapper right-col col-md-7 col-lg-7 col-xl-8">
					<?php // if( $citation != '' ) : ?>
						<div class="element-quote secondary-color">
							<h3 class="title big-title">
								<?php // echo $citation; ?>
							</h3>
						</div>
					<?php // endif; ?>
				</div>
			</div>
		</div>
	</section>-->

	<section class="home--contact cta--banner generic-banner">
		<div class="container">
			<div class="cta--banner__wrapper row align-items-center primary-color--bg">
				<div class="col-md-12 col-lg-6 text-wrapper">
				<?php
					$pretitle_d = false; 
					$title      = $contact['title'];
					$desc       = $contact['desc'];
					$link_array = $contact['link'];
					$link       = $link_array['url'];
					$label      = $link_array['title'];
					include('components/cta_text-block-simple.php');
				?>
				</div>
				<div class="col-md-12 col-lg-5 offset-lg-1 home-cta-image">
					<div class="image-wrapper generic-vignette">
						<?php if ( $contact_image ) : ?>
							<a href="<?php echo $link; ?>" title="<?php echo $label; ?>">
								<img src="<?php echo $contact_image['url']; ?>" class="img-fluid" alt="<?php echo $contact_image['alt']; ?>">
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div> <!-- end container -->
		</div>
	</section>
	
	<!-- post insta -->
	<?php include_once('components/slider-insta.php'); ?>

<?php
get_footer();
