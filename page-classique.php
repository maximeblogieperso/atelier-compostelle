<?php 
/*
	Template Name: Page Classique
*/
get_header(); 
?>

<section class="classic-page">
	<div class="container">
		<div class="row">
			<div class="content-wrapper col-md-12">
				<img src="<?php echo the_post_thumbnail_url(); ?>" class="main-image img-fluid" alt="<?php echo the_title(); ?>">
				<h1 class="title subline text-center">
					<?php echo the_title(); ?>
				</h1>
				<div class="custom-wysiwyg">
					<?php echo the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>			