<?php 
/*
	Template Name: Page Presse
*/
get_header(); 
$pretitle_d = false; 

$introduction  = get_field('introduction');
$desc          = $introduction['desc'];
 
$main_title         = get_field('main-title');
$realisations       = get_field('realisations');
$realisations_image = get_field('realisations-image');
?>

<section class="hero hero--small-hero presse-intro primary-color--bg">
	<div class="container">
		<div class="text-wrapper">
			<h1 class="title big-title">
				<?php echo $introduction['title']; ?>
			</h1>
			<?php if( $desc ) : ?>
				<p class="intro">
					<?php echo $desc; ?>
				</p>
			<?php endif; ?>
		</div>
	</div>
</section>

<section id="main" class="generic-banner listing listing--presse">
	<div class="filters container">
		<div class="row align-items-center">
			<div class="filters__title col-12">
				<h2 class="primary-color big-title title" style="text-align: center;">
					<?php echo $main_title; ?>
				</h2>
			</div>
		</div>
	</div>

	<div class="listing--container">
		<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(  
	        'post_status' => 'publish',
	        'posts_per_page' => 12,
	        'orderby' => 'menu_order', 
	        'order' => 'ASC', 
	        'paged' => $paged,
	    );

	    $loop = new WP_Query( $args ); 
		?>
		<div class="row justify-content-center">
			<?php
				while ( $loop->have_posts() ) : $loop->the_post();
				$cat_name = get_the_category();

				$first_cat = $cat_name[0]->name;
				$cat_name_lower = strtolower($first_cat);
				$tag = get_field('cat');

				$title = get_field('title');
				$canal = get_field('canal');
				$link = get_field('link');
				$date = get_field('date');
			?>
	           <article class="<?php echo $cat_name_lower . '-type'; ?> all-type active listing--container__item col-md-4 col-lg-3">
	           		<?php if ( $link ) : ?>
					<a href="<?php echo $link; ?>" class="image-wrapper" title="Découvrir l'article <?php the_title(); ?>" target="_blank">
						<div class="generic-vignette">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<div class="btn-arrow btn-arrow--right"></div>
						</div>
					</a>
					<?php else : ?>
						<div class="generic-vignette">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<div class="btn-arrow btn-arrow--right"></div>
						</div>
					<?php endif; ?>
					<div class="text-wrapper">
						<h4 class="title small-title">
							<?php echo $title; ?>
						</h4>
						<?php if ( $canal && $date ) : ?>
							<span class="secondary-color">
								<?php echo $canal . ' | ' . $date; ?>
							</span>
						<?php endif; ?>
					</div>
				</article>
		    <?php endwhile; ?>
		    </div> <!-- end row -->
		<?php 
		    $total_pages = $loop->max_num_pages;
		    if ($total_pages > 1) :
		        $current_page = max(1, get_query_var('paged'));
		    ?>
		        <ul class="pagination-custom">
		        <?php echo paginate_links(array(
		            'base' => get_pagenum_link(1) . '%_%',
		            'format' => '/page/%#%',
		            'current' => $current_page,
		            'total' => $total_pages,
		            'prev_text'    => __('« précédente'),
		            'next_text'    => __('suivante »'),
		        ));
		        ?>
		        </ul>
		    <?php endif; ?>   
		    <?php wp_reset_postdata(); ?>
	</div>
</section>

<section class="listing--info white--bg" style="display: none">
	<div class="container">
		<div class="row">
			<div class="col-12 max-width-lg centered">
				<div class="text-wrapper custom-wysiwyg">
					<h3 class="medium-title title ">
						Abonnez-vous à notre infolettre
					</h3>
					<p>
						Tenez-vous informé via notre infolettre hebdomadaire et découvrez les valeurs de l'Atelier Compostelle en suivant notre podcast sur facebook.
					</p>
					<ul class="list-inline">
						<li>
							<a href="https://ateliercompostelle.activehosted.com/f/1" class="btn-primary" rel="noopener" rel="noreferrer" target="_blank" title="Abonnez-vous à notre infolettre">
								Notre infolettre
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/iamanequestrian/videos/2699015927091669/?vh=e&extid=6jNVmNzCicQORb2O" rel="noopener" rel="noreferrer" title="Notre podcast sur facebook" class="btn-link primary-color" target="_blank">
								Notre podcast
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="listing--contact cta--banner generic-banner">
	<div class="container">
		<div class="cta--banner__wrapper row align-items-center flex-row-reverse primary-color--bg">
			<div class="col-md-12 col-lg-7 col-xl-6 offset-xl-1 text-wrapper">
				<?php
					$title      = $realisations['title'];
					$desc       = $realisations['desc'];
					$link_array = $realisations['link'];
					$link       = $link_array['url'];
					$label      = $link_array['title'];
					include('components/cta_text-block-simple.php');
				?>
			</div>
			<div class="col-md-12 col-lg-5">
				<div class="image-wrapper generic-vignette">
					<?php if( $realisations_image ) : ?>
						<a href="<?php $link; ?>" title="<?php echo $label; ?>">
							<img src="<?php echo $realisations_image['url']; ?>" class="img-fluid" alt="<?php echo $realisations_image['alt']; ?>">
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div> <!-- end container -->
	</div>
</section>

<?php get_footer(); ?>			