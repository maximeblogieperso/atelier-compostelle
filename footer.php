<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */
?>

</main><!-- #content -->

<?php include_once('components/slider-partenaire.php'); ?>

<footer class="footer primary-color--bg">
	<div class="container footer--primary">
		<div class="row align-items-start">
			<div class="footer__center logo-mobile">
				<div class="logo-wrapper">
					<a href="<?php echo esc_url(home_url()); ?>" title="<?php bloginfo( 'name' ); ?> accueil">
						<img src="<?php echo esc_url(home_url()); ?>/images/logo-white-alt.svg" class="logo" alt="<?php bloginfo( 'name' ); ?> logo">
					</a>
				</div> 
			</div>

			<div class="footer__left col-lg-4">
				<div class="text-wrapper">
					<span class="small-title title">
						<?php bloginfo( 'name' ); ?>
					</span>
					<span class="footer-text">
						Rittrati di living
					</span>
					<span class="footer-text">
						Paris - Luxembourg
					</span>

					<?php include('components/social-media.php'); ?>
				
				</div>
			</div>

			<div class="footer__center col-lg-4 logo-desktop">
				<div class="logo-wrapper">
					<a href="<?php echo esc_url(home_url()); ?>" title="<?php bloginfo( 'name' ); ?> accueil">
						<img src="<?php echo esc_url(home_url()); ?>/images/logo-white-alt.svg" class="logo" alt="<?php bloginfo( 'name' ); ?> logo">
					</a>
				</div> 
			</div>

			<div class="footer__right col-lg-4">
				<div class="text-wrapper">
					<h4 class="small-title title">
						Un projet de rénovation ?
					</h4>
					<span class="footer-text">
						Contactez Amandine Maroteaux et concrétisez votre projet
					</span>
					<div class="btn-wrapper">
						<a href="https://www.ateliercompostelle.com/contact/" class="btn-secondary--right" title="<?php bloginfo( 'name' ); ?> page contact">
							Contacter l'atelier
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container footer--mentions">
		<div class="row">
			<div class="footer__left col-md-8">
				<span class="footer-text">
		    		© copyright <?php bloginfo( 'name' ); ?> 2020 | <a href="#">Politique de confidentialité</a> - <a href="#">Politique de cookies </a>
		    	</span>
			</div>
			<div class="footer__right col-md-4">
				<span class="footer-text">
					Design & integration par <a href="https://www.maximeblogie.be" title="Maxime Blogie, Graphiste web et print" target="_blank">
						Maxime Blogie
					</a>
				</span>
			<div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
