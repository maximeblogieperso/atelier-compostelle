<?php
/**
 *  Maxime blogie theme functions - based on noto
 *
 */

if(!function_exists('wp_dump')) :
    function wp_dump(){
        if(func_num_args() === 1)
        {
            $a = func_get_args();
            echo '<pre>', var_dump( $a[0] ), '</pre><hr>';
        }
        else if(func_num_args() > 1)
            echo '<pre>', var_dump( func_get_args() ), '</pre><hr>';
        else
            throw Exception('You must provide at least one argument to this function.');
    }
endif;

if ( ! function_exists( 'noto_simple_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function noto_simple_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Noto Simple, use a find and replace
		 * to change 'noto-simple' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'noto-simple', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'noto-simple' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'noto_simple_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'noto_simple_setup' );

function wpdocs_remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'wpdocs_remove_menus' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'noto_simple_content_width', 640 );
}
add_action( 'after_setup_theme', 'theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar'),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Ajoutez les widgets.' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

/**
 * Registers an editor stylesheet for the theme.
 */

// purge the content
add_filter('use_block_editor_for_post', '__return_false', 10);

/**
 * Proper way to enqueue scripts and styles
 */
function custom_styles() {
	wp_enqueue_style( 'style-main', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/app.css' );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/app.js' );
}
add_action( 'wp_enqueue_scripts', 'custom_styles' );

add_filter( 'wp_enqueue_scripts', 'change_default_jquery', PHP_INT_MAX );
function change_default_jquery( ){
    wp_dequeue_script( 'jquery');
    wp_deregister_script( 'jquery');   
}

// Custom post type creation 
function wpm_custom_post_type() {

    // On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
    $labels = array(
        // Le nom au pluriel
        'name'                => _x( 'Projets', 'Post Type General Name'),
        'singular_name'       => _x( 'Projet', 'Post Type Singular Name'),
        'menu_name'           => __( 'Projets'),
        'all_items'           => __( 'Tous les projets'),
        'view_item'           => __( 'Voir les projets'),
        'add_new_item'        => __( 'Ajouter un projet'),
        'add_new'             => __( 'Ajouter'),
        'edit_item'           => __( 'Editer le projet'),
        'update_item'         => __( 'Mettre à jour le projet'),
        'search_items'        => __( 'Rechercher un projet'),
        'not_found'           => __( 'Non trouvé'),
        'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
    );
    
    // On peut définir ici d'autres options pour notre custom post type
    
    $args = array(
        'label'       => __( 'Projets'),
        'description' => __( 'Tous sur les projets'),
        'labels'      => $labels,
        'supports'    => array( 'title', 'editor', 'thumbnail', ),
        'menu_icon'   => 'dashicons-format-image',
        /* Différentes options supplémentaires
        */
        'show_in_rest'  => true,
        'hierarchical'  => false,
        'public'        => true,
        'has_archive'   => true,
        'menu_position' => 4,
        'rewrite'       => array( 'slug' => 'projets'),
    );
    
    // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
    register_post_type( 'projets', $args );
}

add_action( 'init', 'wpm_custom_post_type', 0 );

/* ajout taxonomies */
add_action( 'init', 'wpm_add_taxonomies', 0 );

function wpm_add_taxonomies() {
    $labels_cat = array(
        'name'                       => _x( 'Types de projets', 'taxonomy general name'),
        'singular_name'              => _x( 'Type de projet', 'taxonomy singular name'),
        'search_items'               => __( 'Rechercher une catégorie'),
        'popular_items'              => __( 'Types populaires'),
        'all_items'                  => __( 'Toutes les types de projet'),
        'edit_item'                  => __( 'Editer un type de projet'),
        'update_item'                => __( 'Mettre à jour un type'),
        'add_new_item'               => __( 'Ajouter une nouveau type'),
        'new_item_name'              => __( 'Nom du nouveau type de projet'),
        'add_or_remove_items'        => __( 'Ajouter ou supprimer un type de projet'),
        'choose_from_most_used'      => __( 'Choisir parmi les types les plus utilisées'),
        'not_found'                  => __( 'Pas de type trouvées'),
        'menu_name'                  => __( 'Type de projet'),
    );

    $args_cat = array(
    // Si 'hierarchical' est défini à true, notre taxonomie se comportera comme une catégorie standard
        'hierarchical'          => true,
        'labels'                => $labels_cat,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'type-projet' ),
    );

    register_taxonomy( 'typeprojets', 'projets', $args_cat );
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}