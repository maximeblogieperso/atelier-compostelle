<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-Q4ZEM4GQ29"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-Q4ZEM4GQ29');
	</script>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-v4-grid-only@1.0.0/dist/bootstrap-grid.min.css" type="text/css" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,500;0,700;0,900;1,300;1,500;1,700;1,900&display=swap" rel="stylesheet"> 

	<?php
    wp_head();
        $description = get_bloginfo( 'description', 'display' ); 
        $page_desc = get_field('page-description');
    ?>

    <?php if ($page_desc ) : ?>
        <meta name="description" content="<?php echo $page_desc; ?>">
    <?php endif; ?>

    <!-- pinterest -->
    <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
    <?php
        $slug = get_page_template_slug(); $slug_un = str_replace('.php', '', $slug);
        $title = get_the_title(); $title_un = str_replace(' ', '-', $title);
    ?>
</head>

<body class="<?php echo $slug_un . ' ' . 'page-' . strtolower($title); ?>">
	<header id="masthead" class="site-header">
        <div class="container-fluid">
            <div class="row align-items-lg-center">
                <div class="logo-wrapper col-md-12 col-lg-3 col-xl-3">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo esc_url(home_url()); ?>/images/logo-head-white.svg" class="logo" alt="<?php bloginfo( 'name' ); ?> logo">
                        <span><?php echo $description; ?></span>
                    </a>
                </div><!-- .site-branding -->

                <a id="navicon" href="#" class="btn-nav passive" title="Menu Atelier Compostelle">
                    <span class="lines"></span>
                </a>
                <nav id="main-navigation" class="main-navigation col-md-12 col-lg-9 col-xl-9">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                        ) );
                    ?>
                    <?php include('components/social-media.php'); ?>
                </nav><!-- #site-navigation -->
            </div><!-- row -->
        </div><!-- .container -->
	</header><!-- #masthead -->

	<main id="content" class="site-content">
