<?php 
/*
	Template Name: Page Collab
*/
get_header();

$title       = get_field('title');
$sub_title   = get_field('sub-title');
$intro       = get_field('intro');

$outro_title = get_field('outro-title');
$outro_desc  = get_field('outro-desc');
$outro_link  = get_field('outro-link');

?>

	<section class="hero single-projet primary-color--bg">
		<div class="container">
			<div class="text-wrapper">
				<h1 class="title big-title">
					<?php the_title(); ?>
				</h1>
				<?php if( $sub_title ) : ?>
					<h2 class="sub-title medium-title"><?php echo $sub_title; ?></h2>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="single-projet--main primary-color--bg">
		<div class="container">
			<div class="single-projet__desc row align-items-center flex-row-reverse">
				<!-- col -->
				<div class="col-md-12 col-lg-6 col-image">
					<div class="image-wrapper generic-vignette">
						<img src="<?php the_post_thumbnail_url() ?>" class="img-fluid" alt="<?php the_title(); ?> - <?php bloginfo( 'name' ); ?>">
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="collab--text text-wrapper custom-wysiwyg">
						<div class="intro">
							<h3 class="medium-title title">A propos de la collaboration</h3>
							<?php if( $intro ) : ?>
								<p class="intro">
									<?php echo $intro; ?>
								</p>
							<?php endif; ?>
						</div>

						<ul class="collab-list list-inline">
							<li>
								<a href="#galerie" class="btn btn-secondary--down" title="Découvrir la réalisation en images">
									Découvrir
								</a>
							</li>
							<li class="pinterest-btn">
								<a href="https://www.pinterest.com/pin/create/button/" rel="noopener" rel="nofollow" target="_blank" class="btn btn-icon" title="Enregistrer sur pinterest" data-pin-do="buttonBookmark" data-pin-custom="true">
									<i class="fa fa-pinterest"></i> Epingler
								</a>
							</li>
						</ul>
					</div>
				</div> <!-- end first col -->
			</div>
		</div>
	</section>

	<section id="galerie" class="single-projet--gallery">
		<div class="container">
			<?php if( have_rows( 'landscape' ) ) : ?>
				<div class="full-size row">
					<?php 
						while (have_rows( 'landscape' )) : the_row();
						$image = get_sub_field('image');
						$label = get_sub_field('label');
					?>
						<div class="item col-12">
							<div class="generic-vignette">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['url']; ?>"> 
								<?php if( $label != '' ) : ?>
									<span class="label">
										<?php echo $label; ?>
									</span>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div> <!-- end full size -->
			<?php endif; ?>

			<?php if( have_rows( 'portrait' ) ) : ?>
				<div class="half-size row">
					<?php 
						while (have_rows( 'portrait' )) : the_row();
						$image = get_sub_field('image');
						$label = get_sub_field('label');
					?>
						<div class="item col-md-6">
							<div class="generic-vignette">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['url']; ?>">
								<?php if( $label != '' ) : ?>
									<span class="label">
										<?php echo $label; ?>
									</span>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div> <!-- end half row -->
			<?php endif; ?>
		</div>
	</section>

	<section class="collab--outro">
		<div class="container">
			<div class="collab--outro__wrapper">
				<?php if ( $outro_title ) : ?>
					<h3 class="medium-title white"><?php echo $outro_title; ?></h3>
				<?php endif; ?>

				<?php if ( $outro_desc ) : 
					echo '<p class="white">' . $outro_desc . '</p>'; 
				endif; ?>

				<?php
				if ( $outro_link ) : ?>
				<div class="btn-wrapper">
					<a href="<?php echo $outro_link['url']; ?>" class="btn btn-secondary" rel="noopener, noreferrer, nofollow" target="_blank" title="<?php echo $outro_link['title']; ?>">
						<?php echo $outro_link['title']; ?>
					</a>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<?php if (have_rows('other-colab') ) : ?>
		<section class="collab--other">
			<div class="container">
				<div class="collab--other__wrapper">
				<h3 class="medium-title">Autres collaborations Atelier Compostelle</h3>
					<ul class="collab--other__list">
						<?php
							while(have_rows('other-colab') ) : the_row();
							$item = get_sub_field('link');
							$link = $item['url'];
							$title = $item['title'];
						?>
							<li class="collab--other__list__item">
								<a href="<?php echo $link; ?>" title="<?php echo $title; ?>">
									<?php echo $title; ?>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</section>
	<?php endif; ?>

<?php 
get_footer();
