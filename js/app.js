document.addEventListener("DOMContentLoaded", function() { 
	// menu nav 
	var body = document.body;
	var navicon = document.getElementById('navicon');
	var menu = document.getElementById('main-navigation');

	navicon.onclick = function() {
		if ( navicon.classList.contains('active') ) {
			navicon.classList.replace('active', 'passive');
			body.classList.toggle('menu-open');
			menu.classList.toggle('open');
		} else {
			navicon.classList.replace('passive', 'active');
			body.classList.toggle('menu-open');
			menu.classList.toggle('open');
		}
	}

	// scroll top menu 
	var scrollObject = {};
	window.onscroll = getScrollPosition;
	var head = document.getElementById('masthead');

	function getScrollPosition(){
	    scrollObject = {
	       x: window.pageXOffset,
	       y: window.pageYOffset
	    }
	    if( scrollObject.y > 150 ) {
	        head.classList.add('is-active');
	    } else {
	        head.classList.remove('is-active');
	    }
	}

	// slider partenaires
	var prevBtn = document.getElementById('partenaire-prev');
	var nextBtn = document.getElementById('partenaire-next');
	const partenaireSlider = document.getElementById('partenaire-slider');
	const sliderWrapper = document.getElementById('partenaire-slider-wrapper');
	const refElem = partenaireSlider.querySelector('.references__slider--item');
	const refElemSize = refElem.scrollWidth;

	var sizeSlider = partenaireSlider.scrollWidth;
	var sizeWrapper = sliderWrapper.scrollWidth;
	var scrolledSlider = partenaireSlider.scrollLeft;

	if ( scrolledSlider <= 20 ) {
		prevBtn.classList.add('passive');
	}

	prevBtn.onclick = function() {
		partenaireSlider.scrollLeft -= refElemSize;
		if ( scrolledSlider >= 20 ) {
			prevBtn.classList.remove('passive');
		} else {
			prevBtn.classList.add('passive');
			nextBtn.classList.remove('passive');
		}
	}

	nextBtn.onclick = function() {
		partenaireSlider.scrollLeft += refElemSize;
		prevBtn.classList.remove('passive');
		if ( sizeSlider - sizeWrapper <= 50 ) {
			nextBtn.classList.add('passive');
		}
	}

	// slider insta 
	if ( document.body.classList.contains('page-homepage') || document.body.classList.contains('page-contact') || document.body.classList.contains('page-atelier') ) {
		var prevInstaBtn = document.getElementById('insta-prev');
		var nextInstaBtn = document.getElementById('insta-next');
		const instaSlider = document.getElementById('insta-slider');
		const instaElem = document.querySelector('.insta__slider--item');
		const instaElemSize = instaElem.scrollWidth;

		nextInstaBtn.onclick = function() {
			instaSlider.scrollLeft += instaElemSize;
		}
		prevInstaBtn.onclick = function() {
			instaSlider.scrollLeft -= instaElemSize;
		}
	}

	// slider team
	if ( document.body.classList.contains('page-atelier') ) {
		var prevTeamBtn = document.getElementById('team-prev');
		var nextTeamBtn = document.getElementById('team-next');
		const teamSlider = document.getElementById('team-slider');
		const teamElem = document.querySelector('.team-slider--item');
		const teamElemSize = teamElem.scrollWidth;

		prevTeamBtn.onclick = function() {
			teamSlider.scrollLeft += teamElemSize;
		}
		nextTeamBtn.onclick = function() {
			teamSlider.scrollLeft -= teamElemSize;
		}
	}

	// slider home projets 
	if ( document.body.classList.contains('page-homepage') || document.body.classList.contains('page-atelier')) {
		var prevProjet = document.getElementById('projet-prev');
		var nextProjet = document.getElementById('projet-next');
		const projetSlider = document.getElementById('projet-slider');
		const projetElem = document.querySelector('.projet-slider-item');
		const projetElemSize = projetElem.scrollWidth;

		nextProjet.onclick = function() {
			projetSlider.scrollLeft += projetElemSize;
		}

		prevProjet.onclick = function() {
			projetSlider.scrollLeft -= projetElemSize;
		}
	}

	// filters buttons projets
	//if ( document.body.classList.contains('page-listing') || document.body.classList.contains('page-presse') ) {
	//	var projets_div = document.getElementById('main');
	//	var posts = projets_div.querySelectorAll('article.listing--container__item');
	//	const buttons = document.querySelectorAll( '.btn-filters' );// For each elements enable click
//
	//	if (sessionStorage.getItem("type")) {
	//	    console.log(sessionStorage.getItem("type"));
	//	    const type = sessionStorage.getItem("type");
	//	    var myBtn = document.getElementById(type);
	//	    // retirer si filtre actif
	//	    projets_div.querySelectorAll('article.listing--container__item').forEach( div => {
	//         	div.classList.replace( 'active', 'inactive' );
	//        } );
//
	 //       projets_div.querySelectorAll( '.' + type + '-type' ).forEach( div => {
	//            div.classList.replace( 'inactive', 'active' );
	  //      } );
//
	//        buttons.forEach( el => {
	//	       	el.classList.remove('active');
	//	    });
	//	    myBtn.classList.add('active');
	//	}
//
	//	buttons.forEach( element => {
	//	    element.addEventListener( 'click', (e) => {
	//	        // Get all element with targeted class
	//	        buttons.forEach( el => {
	//	        	el.classList.remove('active');
	//	        });
	//	        element.classList.add('active');
//
	//	        sessionStorage.clear();
	//            sessionStorage.setItem('type', element.id);
	//            console.log(sessionStorage.getItem("type"));
//
	//	        projets_div.querySelectorAll('article.listing--container__item').forEach( div => {
	//	         	div.classList.replace( 'active', 'inactive' );
	//	         } );
	//	        projets_div.querySelectorAll( '.' + element.id + '-type' ).forEach( div => {
	//	            div.classList.replace( 'inactive', 'active' );
	//	        } );
	//	    } );
	//	} );
	//}
});