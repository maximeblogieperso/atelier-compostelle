<div class="cta cta--text-block cta--simpte custom-wysiwyg">
	<?php if( $title != '' ) : ?>
		<h2 class="medium-title title-lines title">
			<?php echo $title; ?>
		</h2>
		<?php
	endif; 
	if( $desc != '' ) : ?>
		<p><?php echo $desc; ?></p>
		<?php
	endif; 
	if( $link != '' && $label != '' ) : ?>
		<a href="<?php echo $link;?>" class="btn" title="<?php echo $label; ?>">
			<?php echo $label; ?>
		</a>
	<?php endif; ?>
</div>