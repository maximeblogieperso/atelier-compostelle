<?php
if($pre_title != '' ) :
	$title_class = 'medium-title';
else :
	$title_class = 'big-title title-lines';
endif;
?>

<div class="cta cta--text-block custom-wysiwyg">
	<?php
	if( $title != '' ) : ?>
		<h1 class="big-title title-lines title">
			<?php echo $title; ?>
		</h1>
		<?php
	endif; 
	if( $desc != '' ) : ?>
		<p><?php echo $desc; ?></p>
		<?php
	endif; 
	if( $link != '' && $label != '' ) : ?>
		<a href="<?php echo $link;?>" class="btn" title="<?php echo $label; ?>">
			<?php echo $label; ?>
		</a>
	<?php endif; ?>
</div>

<?php 
	// reset data 
	$pre_title = '';
?>