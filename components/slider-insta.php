<?php

$insta     = get_field('link-instagram', 'option' );
$pinterest = get_field('link-pinterest', 'option' );
$youtube   = get_field('link-youtube', 'option');

if ( have_rows( 'insta-posts', 'option' ) ) : ?>
	<aside class="insta white--bg">
		<div class="container-fluid">
			<div class="row align-items-center">

				<div class="col-md-12 col-lg-5">
					<div class="text-wrapper">
						<h4 class="big-title title">
							<?php echo get_field('title-reseau', 'option'); ?>
						</h4>
						<p>
							<?php echo get_field('desc-reseau', 'option'); ?>
						</p>
						<ul class="btn-wrapper list-inline">
							<li class="list-inline-item">
								<a href="<?php echo $pinterest; ?>" class="btn-icon secondary-color" target="_blank"  title="Pinterest <?php bloginfo( 'name' ); ?>">
									<i class="fa fa-pinterest"></i>
								</a>
							</li>
							<li class="list-inline-item">
								<a href="<?php echo $insta; ?>" class="btn-icon secondary-color" target="_blank" title="Instagram <?php bloginfo( 'name' ); ?>">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<?php if ($youtube) : ?>
							<li class="list-inline-item">
								<a href="<?php echo $youtube; ?>" class="btn-icon secondary-color" target="_blank" title="Youtube <?php bloginfo( 'name' ); ?>">
									<i class="fa fa-youtube-play"></i>
								</a>
							</li>
						<?php endif; ?>
						</ul>
					</div>
				</div>

				<div id="insta-slider-wrapper" class="horizontal-slider-wrapper col-md-12 col-lg-7">
					<div id="insta-slider" class="insta__slider horizontal-slider disable-scrollbars">
						<?php
							while( have_rows( 'insta-posts', 'option' ) ) : the_row();
							$image = get_sub_field('image');
							$link = get_sub_field('link');
							if ($link ) : ?>
							 	<div class="slider-item insta__slider--item generic-vignette">
							 		<a href="<?php echo $link;?>" target="_blank" title="Découvrir la publication sur instagram">
							 			<i class="fa fa-instagram"></i>
							 			<img src="<?php echo $image['url']; ?>" class="img-logo" alt="<?php echo $image['alt']; ?>">
							 		</a>
							 	</div>
							<?php else : ?>
							 	<div class="slider-item insta__slider--item generic-vignette">
							 		<i class="fa fa-instagram"></i>
							 		<img src="<?php echo $image['url']; ?>" class="img-logo" alt="<?php echo $image['alt']; ?>">
							 	</div>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<span id="insta-prev" class="slider-prev-btn" title="Scroller vers la gauche"></span>
					<span id="insta-next" class="slider-next-btn" title="Scroller vers la droite"></span>
				</div>

			</div>
		</div>
	</aside>
	<?php endif; ?>