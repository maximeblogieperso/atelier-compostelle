<?php if ( have_rows( 'items-partenaire', 'option' ) ) : ?>
	<aside class="references white--bg">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-12  col-lg-3 col-xl-2">
					<h4 class="medium-title title">
						Références
					</h4>
				</div>
				<div id="partenaire-slider-wrapper" class="horizontal-slider-wrapper col-md-12 col-lg-9 col-xl-10">
					<div id="partenaire-slider" class="references__slider horizontal-slider disable-scrollbars">
						<?php
							while( have_rows( 'items-partenaire', 'option' ) ) : the_row();
							$logo = get_sub_field('image');
							$link = get_sub_field('link');
							if ($link ) : ?>
							 	<div class="slider-item references__slider--item">
							 		<a href="<?php echo $link;?>" target="_blank" title="Découvrir ce partenaire">
							 			<img src="<?php echo $logo['url']; ?>" class="img-logo" alt="<?php echo $logo['alt']; ?>">
							 		</a>
							 	</div>
							<?php else : ?>
							 	<div class="slider-item references__slider--item">
							 		<img src="<?php echo $logo['url']; ?>" class="img-logo" alt="<?php echo $logo['alt']; ?>">
							 	</div>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<span id="partenaire-prev" class="slider-prev-btn slider-partenaire-btn" title="Scroller vers la gauche"></span>
					<span id="partenaire-next" class="slider-next-btn slider-partenaire-btn" title="Scroller vers la droite"></span>
					
				</div>
			</div>
		</div>
	</aside>
	<?php endif; ?>