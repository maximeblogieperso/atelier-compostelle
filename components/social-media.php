<?php 
	$insta     = get_field('link-instagram', 'option' );
	$youtube   = get_field('link-youtube', 'option');
	$pinterest = get_field('link-pinterest', 'option' );
?>
<ul class="social-media btn-wrapper list-inline">
	<li class="list-inline-item">
		<a href="<?php echo $pinterest; ?>" class="btn-icon" target="_blank"  title="Pinterest <?php bloginfo( 'name' ); ?>">
			<i class="fa fa-pinterest"></i>
		</a>
	</li>
	<li class="list-inline-item">
		<a href="<?php echo $insta; ?>" class="btn-icon" target="_blank" title="Instagram <?php bloginfo( 'name' ); ?>">
			<i class="fa fa-instagram"></i>
		</a>
	</li>
	<?php if ($youtube) : ?>
		<li class="list-inline-item">
			<a href="<?php echo $youtube; ?>" class="btn-icon" target="_blank" title="Youtube <?php bloginfo( 'name' ); ?>">
				<i class="fa fa-youtube-play"></i>
			</a>
		</li>
	<?php endif; ?>
</ul>