<?php
/**
 * Setup query to show the ‘services’ post type with all posts filtered by 'home' category.
 * Output is linked title with featured image and excerpt.
 */
   
    $args = array(  
        'post_type' => 'projets',
        'post_status' => 'publish',
        'posts_per_page' => 5,
        'orderby' => 'menu_order', 
        'order' => 'ASC', 
    );

    $loop = new WP_Query( $args ); 
?>

<section id="projets" class="home--projets primary-color--bg">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="text-wrapper col-lg-6 col-xl-5">
				<?php
					$title      = $realisations['title'];
				    $desc       = $realisations['desc'];
				    $link_array = $realisations['link'];
					$link       = $link_array['url'];
					$label      = $link_array['title'];
					include('hero_text-block.php');
				?>
			</div>
			<div class="horizontal-slider-wrapper image-wrapper col-lg-6 col-xl-7 pr-0">
				<div id="projet-slider" class="horizontal-slider disable-scrollbars">
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				           <article class="slider-item projet-slider-item vignette-projet">
								<a href="<?php the_permalink(); ?>" title="Découvrir le projet <?php the_title(); ?>">
									<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
									<span class="label btn-white--right">
										<?php the_title(); ?>
									</span>
								</a>
							</article>
				    <?php
					    endwhile;
					    wp_reset_postdata();
				    ?>
				</div> <!-- end horizontal scroll -->

				<span id="projet-prev" class="slider-prev-btn" title="Scroller vers la gauche"></span>
				<span id="projet-next" class="slider-next-btn" title="Scroller vers la droite"></span>
			</div>
		</div>
	</div>
</section>
