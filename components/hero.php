<section class="hero hero--generic-hero primary-color--bg">
	<div class="container">
		<div class="row flex-row-reverse">
			<div class="image-wrapper col-md-12 col-lg-6">
				<div class="generic-vignette">
					<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" alt="<?php bloginfo( 'name' ); ?> projet architecture">
				</div>
			</div>
			<div class="text-wrapper col-md-12 col-lg-6">
				<?php
					$title = $introduction['title'];
					$desc  = $introduction['desc'];
					$link  = '#main';
					$label = $introduction['label'];
					include('hero_text-block.php');
				?>
			</div>
		</div>
	</div>
</section>