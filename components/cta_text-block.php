<?php
if( $pretitle_d == true ) :
	$title_class = 'medium-title';
elseif( $pretitle_d == false ) : 
	$title_class = 'big-title title-lines';
endif;
?>

<div class="cta cta--text-block custom-wysiwyg">
	<?php if( $pretitle_d == true ) : ?>
		<span class="wide-title title-lines">
			<?php echo $pre_title; ?>
		</span>
	<?php endif;
	if( $title != '' ) : ?>
		<h2 class="<?php echo $title_class; ?> title">
			<?php echo $title; ?>
		</h2>
		<?php
	endif; 
	if( $desc != '' ) : ?>
		<p><?php echo $desc; ?></p>
		<?php
	endif; 
	if( $link != '' && $label != '' ) : ?>
		<a href="<?php echo $link;?>" class="btn" title="<?php echo $label; ?>">
			<?php echo $label; ?>
		</a>
	<?php endif; ?>
</div>

<?php 
	// reset data 
	$pre_title = '';
?>