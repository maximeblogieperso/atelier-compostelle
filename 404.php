<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Noto_Simple
 */

get_header(); ?>

<section class="hero hero--generic-hero page-404 primary-color--bg">
	<div class="container">
		<div class="row flex-row-reverse">
			<div class="text-wrapper col-md-12 col-lg-8 offset-lg-2">
				<?php
					$title = "Page non trouvée";
					$desc  = 'Cette page n\'existe pas, rendez-vous sur notre page d\'accueil et découvrez les réalisations de l\'Atelier';
					$link  = esc_url( home_url( '/' ) );
					$label = 'Page d\'accueil';
					include('components/hero_text-block.php');
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
