<?php
get_header();

$tag = get_field('cat');

$title = get_field('title');
$canal = get_field('canal');
$link = get_field('link');
$date = get_field('date');

?>

	<section class="hero single-projet primary-color--bg">
		<div class="container">
			<div class="text-wrapper">
				<h1 class="title big-title">
					<?php the_title(); ?>
				</h1>
				<?php if( $tag ) : ?>
					<h2 class="sub-title medium-title"><?php echo $tag[0]; ?></h2>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="single-projet--main primary-color--bg">
		<div class="container">
			<div class="single-projet__desc row align-items-center flex-row-reverse">
				<!-- col -->
				<div class="col-md-12 col-lg-6 col-image">
					<div class="image-wrapper generic-vignette">
						<img src="<?php the_post_thumbnail_url() ?>" class="img-fluid" alt="<?php the_title(); ?> - <?php bloginfo( 'name' ); ?>">
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="text-wrapper custom-wysiwyg">
						<a href="<?php echo esc_url( get_page_link( 9 ) ); ?>" class="btn-link" title="Retour aux listing réalisations">
							Retour au listing
						</a>
						<div class="intro">
							<h3 class="medium-title title">A propos de la publication</h3>
							<?php if( $title ) : ?>
								<p class="intro">
									<?php echo $title . ' publié via ' . $canal . ' en ' . $date; ?>
								</p>
							<?php endif; ?>
						</div>

						<ul class="list-inline">
							<li class="pinterest-btn">
								<a href="https://www.pinterest.com/pin/create/button/" rel="noopener" rel="nofollow" target="_blank" class="btn btn-icon" title="Enregistrer sur pinterest" data-pin-do="buttonBookmark" data-pin-custom="true">
									<i class="fa fa-pinterest"></i> Epingler
								</a>
							</li>
						</ul>
					</div>
				</div> <!-- end first col -->
			</div>
		</div>
	</section>

<?php
	$args = array(  
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'orderby' => 'rand', 
        'order' => 'ASC', 
        'paged' => $paged,
    );

    $loop = new WP_Query( $args ); 
	?>

	<section class="single-projet--listing listing">
		<div class="primary-color--bg listing-intro">
			<div class="listing--container">
				<div class="row align-items-end">
					<div class="col-md-12 col-lg-6 col-xl-7">
						<div class="text-wrapper">
							<h3 class="big-title title">
								Découvrez plus de publications
							</h3>
						</div>
					</div>
					<div class="col-md-12 col-lg-6 col-xl-5">
						<div class="link-wrapper">
							<a href="<?php echo esc_url( get_page_link( 14 ) ); ?>" class="btn-link" title="Vers le listing publications <?php bloginfo( 'name' ); ?>">
								Toutes les publications
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="projets-list listing--container">
			<div class="row justify-content-center">
			<?php
				while ( $loop->have_posts() ) : $loop->the_post();
				$cat_name = get_the_category();

				$first_cat = $cat_name[0]->name;
				$cat_name_lower = strtolower($first_cat);
				$tag = get_field('cat');

				$title = get_field('title');
				$canal = get_field('canal');
				$link = get_field('link');
				$date = get_field('date');
			?>
	           <article class="<?php echo $cat_name_lower . '-type'; ?> all-type active listing--container__item col-md-4 col-lg-4">
	           		<?php if ( $link ) : ?>
					<a href="<?php echo $link; ?>" class="image-wrapper" title="Découvrir l'article <?php the_title(); ?>" target="_blank">
						<div class="generic-vignette">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<div class="btn-arrow btn-arrow--right"></div>
						</div>
					</a>
					<?php else : ?>
						<div class="generic-vignette">
							<img src="<?php the_post_thumbnail_url(); ?>" alt="Photo de <?php the_title(); ?>">
							<div class="btn-arrow btn-arrow--right"></div>
						</div>
					<?php endif; ?>
					<div class="text-wrapper">
						<h4 class="title small-title">
							<?php echo $title; ?>
						</h4>
						<?php if ( $canal && $date ) : ?>
							<span class="secondary-color">
								<?php echo $canal . ' | ' . $date; ?>
							</span>
						<?php endif; ?>
					</div>
				</article>
		    <?php endwhile; ?>
		    <?php wp_reset_postdata(); ?>
		    </div> <!-- end row -->
		</div> <!-- end projects list -->
	</section>

<?php 
get_footer();
